package com.example.qtproject;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;


public class ClassesFragment extends Fragment {

    private ConstraintLayout cl_navbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_classes, container, false);

        cl_navbar = getActivity().findViewById(R.id.cl_navigationbar);
        cl_navbar.setVisibility(view.VISIBLE);

        return view;
    }
}
